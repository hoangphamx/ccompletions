#!/usr/bin/python

format_type = "HOANG" # USER|KERNEL|MACRO
input_file = "data.md"
output_file = "format.md"


with open(input_file, "r+") as file:
    list_func = [];
    list_attr = [];
    list_type = [];

    for line in file:

        # Strip spaces in line
        while True:
            if "  " not in line:
                line = line.replace(" *", '*')
                line = line.replace('*', "* ")
                print(line)
                break;
            else:
                line = line.replace("  ", ' ')
                continue

        params = line.split('(');
        # Function
        type_func_str = params[0]
        type_func_str = type_func_str.rstrip()
        type_func_arr = type_func_str.split(' ')
        func = type_func_arr[-1]
        list_func.insert(len(list_func), type_func_arr[-1])

        # Type
        type_arr = type_func_str.split('{}'.format(func))
        type = type_arr[0]
        type = type.rstrip()
        list_type.insert(len(list_type), type)

        # Arguments
        attr = params[1]
        attr_arr = attr.split(')')
        attr_str = attr_arr[0]
        attr_arr = attr_str.split(',');

        # Args format
        arg_out = "("
        pos = -1
        for attr in attr_arr:
            pos += 1
            attr = attr.lstrip()
            attr = attr.rstrip()

            if pos == 0 and attr == "":
                attr = "void"

            if pos == len(attr_arr)-1:
                arg_out += "${%d:%s});" %(pos+1, attr)
                list_attr.insert(len(list_attr), arg_out)
            else:
                arg_out += "${%d:%s}, " %(pos+1, attr)


    # Output format
    # output = '{ "trigger": "USER %s() \\t%s", "contents": "%s %s %s"}'
    if len(list_func) == len(list_attr) and len(list_func) == len(list_type):
        for i in range(0, len(list_func)):
            output = '{ "trigger": "%s-%s() \\t%s", "contents": "%s %s%s"},' %(format_type, list_func[i], list_type[i], list_type[i], list_func[i], list_attr[i])
            print(output)
    else:
        print("ERROR: Bugs - contact me at pxhoang@live.com")




